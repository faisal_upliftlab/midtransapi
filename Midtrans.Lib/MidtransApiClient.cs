﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace Midtrans.Lib
{
    public class MidtransApiClient
    {
        RestClient client = new RestClient("https://app.sandbox.midtrans.com");
        private string serverKey;

        public MidtransApiClient(string serverKey)
        {
            this.serverKey = serverKey;
        }

        public string GetToken(string orderId = null, string totalPayment = null, string firstName = null, string phone = null, string email = null, string address = null)
        {
            RestRequest request = new RestRequest("snap/v1/transactions", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddBody(new { transaction_details = new { order_id = "ORDER-" + orderId, gross_amount = totalPayment }, customer_details = new { first_name = firstName, phone = phone, email = email, shipping_address = new { address = address } } });

            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", this.serverKey);

            IRestResponse<TokenIdParam> response = client.Execute<TokenIdParam>(request);
            string tokenId = response.Data.token;

            return tokenId;

        }

        public string Payment(string token)
        {
            RestRequest request = new RestRequest("snap/v1/transactions/{token}/pay", Method.POST);

            request.AddUrlSegment("token", token);
            request.RequestFormat = DataFormat.Json;
            //request.AddBody(new { payment_type = "permata_va", payment_params = new { user_id = "raymond" } });
            request.AddBody(new { payment_type = "permata_va" });

            IRestResponse response = client.Execute(request);
            string content = response.Content;

            return content;
        }

        public string SetOrderId()
        {
            string orderId = DateTime.Now.ToString("ddMMhhss");
            return orderId;
        }
    }
}
