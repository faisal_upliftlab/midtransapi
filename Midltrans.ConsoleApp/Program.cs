﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Midtrans.Lib;

namespace Midltrans.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string totalPayment;
            string customerName, customerPhone, customerEmail, customerAddress;
            string serverKey = ConfigurationManager.AppSettings["serverKey"];

            MidtransApiClient midtrans = new MidtransApiClient(serverKey);
            string orderId = midtrans.SetOrderId();
            Console.WriteLine("Order ID : {0}", orderId);
            Console.Write("Total Payment :");
            totalPayment = Console.ReadLine();
            Console.Write("Customer Name :");
            customerName = Console.ReadLine();
            Console.Write("Customer Phone :");
            customerPhone = Console.ReadLine();
            Console.Write("Customer Email :");
            customerEmail = Console.ReadLine();
            Console.Write("Customer Address :");
            customerAddress = Console.ReadLine();
            string token = midtrans.GetToken(orderId, totalPayment, customerName, customerPhone, customerEmail, customerAddress);
            Console.Write("Please do transfer first then press enter to proccesed more futher or just press enter");
            Console.ReadLine();
            Console.WriteLine(midtrans.Payment(token));
            Console.Write("Transaction success, press enter to close...");
            Console.ReadKey();
        }
    }
}
